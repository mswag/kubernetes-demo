

Installation & Setup
====================

Frontend
--------
```
cd src/backend
npm install
```

Backend
-------
```
cd src/frontend
npm install
```


Start Project
=============

Frontend
--------
```
PORT=4000 GUESTBOOK_API_ADDR="localhost:3000" npm run start

```

Backend
-------
```
PORT=3000 GUESTBOOK_DB_ADDR="mongodb+srv://[user]:[pwd]@demo-ifvem.mongodb.net/guestbook?retryWrites=true" npm run start
```

Open Browser
------------

Open Url localhost:4000
